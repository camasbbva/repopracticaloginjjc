//Inicializamos el framework en esta variables constante
const express = require('express');
const app = express();
const port = process.env.PORT || 3000
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get('/apitechu/v1/hello',
// Funcion manejadora con request y response
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg" : "Hola desde API TechU!"});
  }
);

app.get('/apitechu/v1/users',
// Funcion manejadora con request y response
  function(req, res){
    console.log("GET /apitechu/v1/users");

    //Devuelve el fichero json de usuarios
    //res.sendFile('usuarios.json',{root: __dirname});

    //Devuelve el fichero en una variable
    var users = require('./usuarios.json');
    //Para recuperar los queryStrings
    console.log(req.query);

    var result = {};

    if (req.query.$count == "true") {
      console.log("Count needed");
      result.count = users.length;
    }

    result.users = req.query.$top ?
       users.slice(0, req.query.$top) : users;

    res.send(result);

  }
);

app.post('/apitechu/v1/users',
// Funcion manejadora con request y response
  function(req, res){
    console.log("POST /apitechu/v1/users");
    //Las cabeceras que esta llegando desde la peticion cliente
    console.log("first_name es " + req.body.first_name);
    console.log("last_name es " + req.body.last_name);
    console.log("email es " + req.body.email);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email
    };

    var users = require('./usuarios.json');
    users.push(newUser);
    //Se ha factorizado la escritura en fichero en una funcion
    writeUserDataToFile(users);

    console.log("Usuario añadido con éxito");
    res.send({"msg" : "Usuario añadido con éxito"});
  }
);


app.post('/apitechu/v1/login',
// Funcion manejadora con request y response
  function(req, res){

    console.log("email es " + req.body.email);
    console.log("email es " + req.body.password);

    var userLog = {
      "email" : req.body.email,
      "password" : req.body.password,
    };

    var users = require('./usuarios.json');

    var msg = {"msg" : "login incorrecto"};

    for (user of users) {
        console.log(user.id);
        if (user != null && user.email == req.body.email && user.password == req.body.password ) {
            var msg = {
                        "msg" : "login correcto",
                        "id"  : user.id
                      };
            var id = user.id;
            console.log("login correcto");
            user.logged = true;
            break;
        }
    }
    writeUserDataToFile(users);

    res.send(msg);

  }
);

app.post('/apitechu/v1/logout/',
// Funcion manejadora con request y response
  function(req, res){

    console.log("email es " + req.body.id);

    var userLog = {
      "email" : req.body.id
    };

    var users = require('./usuarios.json');

    var msg = {"mensaje" : "logout incorrecto"};

    for (user of users) {
        if (user != null && user.id == req.body.id && user.logged == true ) {
            var msg = {
                        "mensaje" : "logout correcto",
                        "idUsuario"  : user.id
                      };
            console.log("logout correcto");
            delete user.logged;
            break;
        }
    }
    writeUserDataToFile(users);

    res.send(msg);

  }
);

app.delete('/apitechu/v1/users/:id',
  function(req, res) {
    console.log("DELETE /apitechu/v1/users/:id");
    console.log("id es " + req.params.id);

    var users = require('./usuarios.json');
    //Los arrays empiezan en cero
    //users.splice(req.params.id - 1, 1); //lo quita de la lista

    /*Mi solucion*/
    var valorId = req.params.id;
    for (user of users) {
        console.log(user.id);
        if (user != null && user.id == valorId) {
            //Para borrar con un contador y haces el splice de la posicion
            //users.splice(i, 1);
            delete users[user.id - 1];
            console.log("Usuario con id igual a " + valorId + " en la posicion " + user.id + " borrado");
            break;
        }
    }

    /*Solucion 1 del profe*/
    /*for (user of users){
      console.log("Length of array is " + users.length);
      if (user != null && user.id == req.params.id){
        console.log("La id coincide");
        delete users[user.id - 1];
        break;
      }
    }*/

    /*Solucion 2 profe*/
    /*for (arrayId in users) {
     console.log("posición del array es " + arrayId);
     if (users[arrayId].id == req.params.id) {
       console.log("La id coincide");
       users.splice(arrayId, 1);
       break;
     }
   }*/

    /*Solucion 3 profe, el forEach no tiene break*/
    /*users.forEach(function (user, index) {
    if (user.id == req.params.id) {
      console.log("La id coincide");
      users.splice(index, 1);
    }
    });*/

    //Hay que persistir en el fichero el cambio
    writeUserDataToFile(users);
    console.log("Usuario Borrado");
    res.send({"msg" : "Usuario borrado con éxito"});
  }
);

function writeUserDataToFile(data){
  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
  function(err){
    if (err){
      console.log(err);
    }else{
      console.log("Datos escritos en fichero.");
    }
  }
)
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query Strings");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

  }
)
